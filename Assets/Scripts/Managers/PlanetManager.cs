﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlanetManager : MonoBehaviour
{
	public static PlanetManager Instance;

	public List<Planet> AllPlanets { get; set; }
	public Planet SelectedPlanet { get; private set; }
	public static Action<Planet> OnPlanetSelectionChanged;

	private Planet[] _planets;

	private void Awake()
	{
		Instance = this;
		AllPlanets = FindObjectsOfType<Planet>().ToList();
	}

	private void Start()
	{
		_planets = FindObjectsOfType<Planet>();

		Selectable.OnSelected += p =>
		{
			var selectedPlanet = p?.GetComponent<Planet>();
			SelectedPlanet = selectedPlanet;
			OnPlanetSelectionChanged?.Invoke(selectedPlanet);
		};
	}
}
