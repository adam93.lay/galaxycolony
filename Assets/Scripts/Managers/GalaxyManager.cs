﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GalaxyManager : MonoBehaviour, ISaveState
{
	public static GalaxyManager Instance;

	public GalaxyData GalaxyData;

	public static Action<StarSystem> OnStarSystemChanged;

	public List<StarSystem> AllStarSystems = new List<StarSystem>();

	private void Awake()
	{
		Instance = this;

		SaveManager.Register(this);
	}

	public void SaveState(GlobalState state)
	{
		state.GalaxyData = GalaxyData;
	}
}
