﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public static GameManager Instance { get; private set; }

	public Ship Ship;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		// TODO: Do something about this...
		//Planet startPlanet = PlanetManager.Instance.AllPlanets.First(p => p.Id == 1);
		//ColonyManager.Instance.CreateColony(startPlanet);
	}

	private void Update()
	{
		// DEBUG Time controls...
		if (Input.GetKeyDown(KeyCode.Space))
		{
			Time.timeScale = 0;
		}
		if (Input.GetKeyDown(KeyCode.KeypadPlus))
		{
			Time.timeScale += 1;
		}
		if (Input.GetKeyDown(KeyCode.KeypadMinus))
		{
			Time.timeScale -= 1;
		}

		// SAVE
		if (Input.GetKeyDown(KeyCode.S))
		{
			SaveManager.SaveGame();
		}

		// LOAD
		if (Input.GetKeyDown(KeyCode.L))
		{
			SaveManager.LoadGame();
			SceneManager.LoadScene(2);
		}
	}

	public void LoadGameFromSave()
	{

	}

	// private Vector2 regionSize = Vector2.one * 1000;
	// private float radius = 50f;
	// private List<Vector2> points;

	// private void OnValidate()
	// {
	//  points = PoissonDiscSampling.GeneratePoints(radius, regionSize, 12);
	// }
	//private void OnDrawGizmos()
	// {
	//  Gizmos.DrawWireCube(regionSize/2, regionSize);
	//	if (points == null)
	//		return;
	//	foreach (Vector2 point in points)
	//	{
	//		Gizmos.DrawSphere(point, 20);
	//	}
	// }
}
