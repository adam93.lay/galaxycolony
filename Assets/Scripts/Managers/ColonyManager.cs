﻿using UnityEngine;

public class ColonyManager : MonoBehaviour
{
  public static ColonyManager Instance { get; private set; }

  public GameObject ColonyPrefab;

  private void Awake()
  {
    Instance = this;
  }

  public Colony CreateColony(Planet planet)
  {
    // Is there already a colony on this planet?
    if (planet.GetComponentInChildren<Colony>() != null)
	    return null;

    GameObject colonyObject = Instantiate(ColonyPrefab, planet.transform);

    var colony = colonyObject.GetComponent<Colony>();
    var resourceStore = colonyObject.GetComponent<ResourceStore>();

    int resourceCount = planet.AvailableResourceTypes.Length;
    resourceStore.Resources = new Resource[1 + resourceCount];

    resourceStore.Resources[0] = new Resource {Type = ResourceTypes.Population, MaxValue = 10 };

    for (int i = 0; i < resourceCount; i++)
    {
      ResourceTypes type = planet.AvailableResourceTypes[i];
      resourceStore.Resources[i+1] = new Resource { Type = type }; // +1 to offset pop resource
      var generator = colonyObject.AddComponent<ResourceGenerator>();
      generator.Init(resourceStore, type);
    }

    return colony;
  }
}
