﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
	public static SelectionManager Instance { get; private set; }

	public Selectable CurrentSelection { get; private set; }

	void Awake()
	{
		Instance = this;

		Selectable.OnSelected += s =>
		{
			Debug.Log("Selected: " + s.name);

			Selectable previousSelection = CurrentSelection;

			// Set new selection
			CurrentSelection = s;

			// Don't deselect by default if we're selecting the already selected object
			// (Selectable will do that for us + not always desired behaviour)
			if (previousSelection != s)
			{
				// Deselect the existing selection
				previousSelection?.Deselect();
			}
		};

		Selectable.OnDeselected += s =>
		{
			Debug.Log("Deselected: " + s.name);
			if (CurrentSelection == s)
			{
				CurrentSelection = null;
			}
		};
	}
}
