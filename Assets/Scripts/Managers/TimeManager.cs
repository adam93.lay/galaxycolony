﻿using System;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
  public static TimeManager Instance { get; private set; }
  public static Action<int> OnCycleChanged;

  public int CurrentCycle { get; private set; } = 1;

  public float CycleTime = 1f;

  private void Awake()
  {
    Instance = this;
  }

  private float _t;

  private void Update()
  {
    _t += Time.deltaTime;
    if (_t > CycleTime)
    {
      _t = 0; // Reset time;
      CurrentCycle++;
      OnCycleChanged?.Invoke(CurrentCycle);
    }
  }
}
