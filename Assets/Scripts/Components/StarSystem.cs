﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class StarSystem : MonoBehaviour
{
	public static Action<StarSystem> OnStarSystemExplored;
	public static Action<StarSystem> OnStarSystemVisible;

	public StarSystemData Data;

	public List<StarSystem> Connections = new List<StarSystem>();

	private void Start()
	{

	}

	public void Explore()
	{
		if (Data.Explored)
		{
			Debug.LogWarning("Trying to explore system already explored!", this);
			return;
		}

		Data.Explored = true;

		DiscoverChildren();

		OnStarSystemExplored?.Invoke(this);

		foreach (StarSystem starSystem in Connections)
			if (!starSystem.Data.Visible)
				starSystem.MakeVisible();
	}

	public void MakeVisible()
	{
		if (Data.Visible)
		{
			Debug.LogWarning("Trying to make visible a system already visible!", this);
			return;
		}

		Data.Visible = true;

		DiscoverChildren();

		OnStarSystemVisible?.Invoke(this);
	}

	private void DiscoverChildren()
	{
		// Discover children!
		foreach (Discoverable child in GetComponentsInChildren<Discoverable>())
		{
			child.ToggleVisibility(this);
		}
	}
}
