﻿using System;
using TMPro;
using UnityEngine;

public class Planet : MonoBehaviour
{
	public PlanetData Data;
  public Colony Colony;
  public ResourceTypes[] AvailableResourceTypes;

  [SerializeField] 
  private PlanetHoverUI _ui;
  private SpriteRenderer _sr;
  private TrailRenderer _tr;
  private Discoverable _discoverable;


  void Awake()
  {
	  _sr = GetComponent<SpriteRenderer>();
    _tr = GetComponent<TrailRenderer>();
    _discoverable = GetComponent<Discoverable>();

    _discoverable.OnVisible += (visible) =>
    {
      _ui.Visible = visible;
      _tr.emitting = visible;
    };
  }

  private void Start()
  {
	  var tt = GetComponent<TooltipOnMouseover>();
	  if (tt != null)
	  {
		  tt.Text = Data.Name;
	  }
  }
}
