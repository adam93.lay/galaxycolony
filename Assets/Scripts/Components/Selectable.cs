﻿using System;
using UnityEngine;

public class Selectable : MonoBehaviour
{
	public static Action<Selectable> OnSelected;
	public static Action<Selectable> OnDeselected;

	private bool _isSelected;
	private SpriteRenderer _sr;

	public bool IsSelected
	{
		get => _isSelected;
		private set
		{
			_isSelected = value;

			_sr.material.SetFloat("_hover", _isSelected ? 1.0f : 0.0f);
		}
	}

	void Start()
	{
		_sr = GetComponent<SpriteRenderer>();
	}

	private void OnMouseDown()
	{
		if (IsSelected)
		{
			Deselect();
		}
		else
		{
			Select();
		}
	}

	public void Select()
	{
		IsSelected = true;

		OnSelected?.Invoke(this);
	}

	public void Deselect()
	{
		if (!IsSelected)
			return;

		IsSelected = false;

		OnDeselected?.Invoke(this);
	}
}
