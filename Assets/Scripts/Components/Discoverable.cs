﻿using System;
using UnityEngine;

public class Discoverable : MonoBehaviour
{
  public bool RequireExplored = true;

  public Action<bool> OnVisible;

  [SerializeField]
  private bool _visible;

  private SpriteRenderer _sr;
  private TrailRenderer _tr;
  private LineRenderer _lr;

  void Awake()
  {
    _sr = GetComponent<SpriteRenderer>();
    _tr = GetComponent<TrailRenderer>();
    _lr = GetComponent<LineRenderer>();
  }

  public void ToggleVisibility(StarSystem starSystem) => ToggleVisibility(starSystem.Data.Explored, starSystem.Data.Visible);
  public void ToggleVisibility(bool systemExplored, bool systemVisible)
  {
    bool visible = systemExplored || systemVisible && !RequireExplored;

    if (visible == _visible)
      return;

    _visible = visible;

    if (_sr != null)
      _sr.enabled = _visible;
    if (_tr != null)
      _tr.enabled = _visible;
    if (_lr != null)
      _lr.enabled = _visible;

    OnVisible?.Invoke(_visible);
  }
}
