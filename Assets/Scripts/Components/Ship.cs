﻿using UnityEngine;

[RequireComponent(typeof(RotateAroundTarget))]
public class Ship : MonoBehaviour
{
	public ShipData Data;

	public float EnterOrbitRadius;

	private RotateAroundTarget _rotateAroundTarget;

	private void Start()
	{
		_rotateAroundTarget = GetComponent<RotateAroundTarget>();
		_selectable = GetComponent<Selectable>();
		//_rotateAroundTarget.enabled = false;
	}

	private Transform _target;
	private Planet _orbitingPlanet;
	private Selectable _selectable;
	private float _timeSinceProbe;
	private float _probeTime = 1.0f;
	private float _probeRange = 10.0f;

	void Update()
	{
		// Input
		if (!_selectable.IsSelected)
			return;

		_timeSinceProbe += Time.deltaTime;

		if (_timeSinceProbe > _probeTime)
		{
			_timeSinceProbe = 0;

			Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, _probeRange);

			foreach (Collider2D hit in hits)
			{
				var starSystem = hit.GetComponent<StarSystem>();
				if (starSystem != null)
				{
					if (!starSystem.Data.Explored)
					{
						Debug.Log("Ship found a new star system! " + starSystem.Data.Name);
						starSystem.Explore();
					}
				}
			}
		}

		if (Input.GetMouseButtonDown(1))
		{
			ExitOrbit();

			Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			StopAllCoroutines();
			StartCoroutine(transform.TurnToLook(mousePos, 0.25f, -90f));

			RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
			var target = hit.transform?.GetComponent<Planet>();
			if (target != null)
			{
				_target = target.transform;
			}
			else
			{
				_target = null;
				StartCoroutine(transform.MoveTo(mousePos, Data.Speed));
			}
		}

		if (_target != null)
		{
			if (!EnterOrbit(_target))
			{
				transform.position = Vector2.MoveTowards(transform.position, _target.position, Time.deltaTime * Data.Speed);
			}
		}
	}

	private bool EnterOrbit(Transform target)
	{
		Vector2 difference = target.position - transform.position;
		if (difference.magnitude > EnterOrbitRadius)
			return false;

		Vector2 lookDirection = Vector2.Perpendicular(difference);
		transform.rotation = transform.TopDownLookAt((Vector2)transform.position + lookDirection, 90f);

		float degrees = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
		_rotateAroundTarget.SetTimeFromSignedAngle(degrees);
		_rotateAroundTarget.Target = _target;
		_rotateAroundTarget.enabled = true;
		_target = null;

		_orbitingPlanet = target.GetComponent<Planet>();

		return true;
	}

	private void ExitOrbit()
	{
		_rotateAroundTarget.Target = null;
		_rotateAroundTarget.enabled = false;
		_orbitingPlanet = null;
	}

	public void CreateColonyOnOrbitingPlanet()
	{
		// We're not orbiting a planet
		if (_orbitingPlanet == null)
			return;

		ColonyManager.Instance.CreateColony(_orbitingPlanet);
	}
}
