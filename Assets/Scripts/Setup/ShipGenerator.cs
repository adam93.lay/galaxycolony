﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;

public class ShipGenerator : MonoBehaviour
{
	public static ShipGenerator Instance { get; private set; }

	public static Action<Ship> OnShipCreated;

	public GameObject ShipPrefab;

	private Sprite[] _shipSprites;

	void Awake()
	{
		Instance = this;

		_shipSprites = Resources.LoadAll<Sprite>("Images/Ships");

		EmpireGenerator.OnEmpiresCreated += empireData =>
		{
			foreach (EmpireData empire in empireData)
			{
				foreach (var ship in empire.Ships)
				{
					Ship goShip = CreateShipFromData(ship);
					goShip.transform.position = empire.HomeSystem.OriginPoint - Vector2.one * 1.5f;
				}
			}

			Debug.Log("Ships Created");
		};
	}

	public ShipData GenerateStartingShip(EmpireData owner)
	{
		return new ShipData
		{
			Id = 0,
			Name = "Starting Ship",
			Speed = 10,
			Type = ShipTypes.Standard,
			OwnerEmpireId = owner.Id,
			Owner = owner
		};
	}

	public Ship CreateShipFromData(ShipData shipData)
	{
		// Create new ship from prefab
		GameObject goShip = Instantiate(ShipPrefab);

		var shipSpriteRenderer = goShip.GetComponent<SpriteRenderer>();
		var ship = goShip.GetComponent<Ship>();

		// Attach planet data
		ship.Data = shipData;

		// Assign name to gameobject from data
		ship.name = shipData.Name;

		// Assign sprite for planet
		var spriteName = shipData.Type.ToString();
		shipSpriteRenderer.sprite = _shipSprites.First(sp => sp.name == spriteName);

		OnShipCreated?.Invoke(ship);

		return ship;
	}
}
