﻿using System.Collections.Generic;
using UnityEngine;

public static class NameGenerator
{
	private static readonly List<string> _history = new List<string>();

	private static readonly char[] _suffixLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
	private static readonly char[] _suffixNumbers = "0123456789".ToCharArray();
	private static readonly string[] _names =
	{
		"Ophelia",
		"Titania",
		"Portia",
		"Venus",
		"Pandora",
		"Apollo",
		"Saros",
		"Oberon",
		"Sirius"
	};

	public static string GenerateName(int randomSeed)
	{
		var random = new System.Random(randomSeed);

		string GetName()
		{
			string name = _names[random.Next(_names.Length)];
			name += "-";
			name += _suffixLetters[random.Next(_suffixLetters.Length)];
			name += _suffixNumbers[random.Next(_suffixNumbers.Length)];
			name += _suffixNumbers[random.Next(_suffixNumbers.Length)];
			return name;
		}

		// Generate names until we get a new one
		string newName;
		do
		{
			newName = GetName();
		} while (_history.Contains(newName));

		_history.Add(newName);

		return newName;
	}
}
