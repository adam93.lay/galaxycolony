﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EmpireGenerator : MonoBehaviour
{
	public static Action<EmpireData[]> OnEmpiresCreated;

	void Awake()
	{
		GalaxyGenerator.OnGalaxyCreated += galaxyData =>
		{
			EmpireData[] data = null;

			if (SaveManager.LoadState != null)
			{
				data = SaveManager.LoadState.Empires;
				//GenerateEmpiresFromData(data);
			}
			else
			{
				data = GenerateNewEmpires();
			}

			Debug.Log("Empires Created");

			OnEmpiresCreated?.Invoke(data);
		};
	}

	private EmpireData[] GenerateNewEmpires()
	{
		// Pick starting system
		List<StarSystem> allStarSystems = GalaxyManager.Instance.AllStarSystems;

		// Pick random home system
		StarSystem homeSystem = allStarSystems[Random.Range(0, allStarSystems.Count)];

		// Start with home system explored
		homeSystem.MakeVisible();
		homeSystem.Explore();

		var playerEmpire = new EmpireData
		{
			Id = 0,
			Name = "Player Empire",
			HomeSystemId = homeSystem.Data.Id,
			HomeSystem = homeSystem.Data,
			Ships = new ShipData[1]
		};

		// Generate starting ship
		playerEmpire.Ships[0] = ShipGenerator.Instance.GenerateStartingShip(playerEmpire);

		// Create space station in home system
		SpaceStationGenerator.Instance.CreateSpaceStation(homeSystem, playerEmpire);

		// Focus on home system
		Camera.main.transform.position = new Vector3(homeSystem.transform.position.x, homeSystem.transform.position.y, -10);

		return new[]
		{
			playerEmpire
		};
	}

	// TODO: Do we physically create anything for the empire?
	//private void GenerateEmpiresFromData(EmpireData[] empires)
	//{
	//}
}
