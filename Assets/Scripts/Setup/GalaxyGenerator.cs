﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class GalaxyGenerator : MonoBehaviour
{
	public GameObject StarSystemPrefab;
	public GameObject PlanetPrefab;
	public GameObject HyperlanePrefab;
	public GameObject AsteroidPrefab;

	public static Action<GalaxyData> OnGalaxyCreated;

	private GameObject _space;
	private Sprite[] _planetSprites;
	private Sprite[] _starSprites;
	private Sprite[] _asteroidSprites;

	private void Start()
	{
		_planetSprites = Resources.LoadAll<Sprite>("Images/Planets");
		_starSprites = Resources.LoadAll<Sprite>("Images/Stars");
		_asteroidSprites = Resources.LoadAll<Sprite>("Images/Asteroids");
		_space = GameObject.Find("Space");

		GalaxyData galaxyData = null;

		if (SaveManager.LoadState == null)
		{
			galaxyData = GenerateNewGalaxy();
		}
		else
		{
			galaxyData = GenerateGalaxyFromData(SaveManager.LoadState.GalaxyData);
		}

		// Create connections between star systems
		GenerateStarSystemConnections(GalaxyManager.Instance.AllStarSystems);

		Debug.Log("Galaxy Created");

		// Notify complete
		OnGalaxyCreated?.Invoke(galaxyData);
	}

	private int _lastStarSystemId;
	private int _lastPlanetId;

	private GalaxyData GenerateNewGalaxy()
	{
		var size = 200;
		List<Vector2> points = PoissonDiscSampling.GeneratePoints(30, new Vector2(size, size), 12);

		var galaxyData = new GalaxyData
		{
			StarSystems = new StarSystemData[points.Count]
		};

		_lastStarSystemId = 0;
		_lastPlanetId = 0;

		for (var i = 0; i < points.Count; i++)
		{
			Vector2 point = points[i];
			var starSystemOrigin = new Vector2(point.x - size / 2, point.y - size / 2);

			galaxyData.StarSystems[i] = GenerateNewStarSystem(starSystemOrigin, i);

			GameObject goStarSystem = GenerateStarSystemFromData(_space.transform, galaxyData.StarSystems[i]);

			GalaxyManager.Instance.AllStarSystems.Add(goStarSystem.GetComponent<StarSystem>());
		}

		// Start with nothing visible
		foreach (var starSystem in GalaxyManager.Instance.AllStarSystems)
			foreach (Discoverable child in starSystem.GetComponentsInChildren<Discoverable>())
				child.ToggleVisibility(false, false);

		return galaxyData;
	}

	private StarSystemData GenerateNewStarSystem(Vector2 origin, int starSystemIndex)
	{
		StarSystemTypes[] starSystemTypes = Enum.GetValues(typeof(StarSystemTypes))
			.Cast<StarSystemTypes>().ToArray();

		var starSystem = new StarSystemData
		{
			Id = _lastStarSystemId++,
			Name = NameGenerator.GenerateName(starSystemIndex * 1_000_000),
			OriginPoint = (Vector3)origin,
			StarSize = Random.Range(1.0f, 1.2f),
			Type = starSystemTypes[Random.Range(0, starSystemTypes.Length)]
		};

		starSystem.Planets = Enumerable
			.Range(0, Random.Range(3, 7))
			.Select(i => GenerateNewPlanet(starSystem, i))
			.ToArray();

		return starSystem;
	}

	private PlanetData GenerateNewPlanet(StarSystemData starSystem, int planetIndex)
	{
		PlanetTypes[] planetTypes = Enum.GetValues(typeof(PlanetTypes))
			.Cast<PlanetTypes>().ToArray();

		return new PlanetData
		{
			Id = _lastPlanetId++,
			Name = NameGenerator.GenerateName(planetIndex),
			Size = Random.Range(0.5f, 1.0f),
			Type = planetTypes[Random.Range(0, planetTypes.Length)],
			OrbitRadius = (1.5f + planetIndex) * 2f,
			OrbitSpeed = Random.Range(3.0f, 6.0f),
			StarSystemId = starSystem.Id,
			StarSystem = starSystem
		};
	}

	private GameObject GenerateNewAsteroid(GameObject goStarSystem)
	{
		GameObject goAsteroid = Instantiate(AsteroidPrefab, goStarSystem.transform);

		// Random size
		float size = Random.Range(0.15f, 0.3f);
		goAsteroid.transform.localScale = new Vector2(size, size);

		// Assign sprite for new planet
		var asteroidSpriteRenderer = goAsteroid.GetComponent<SpriteRenderer>();
		asteroidSpriteRenderer.sprite = _asteroidSprites[Random.Range(0, _asteroidSprites.Length)];

		// Generate random start position
		float radius = Random.Range(3, 15);
		ApplyStartPointInOrbit(goAsteroid, radius);

		// Set random orbit speed
		goAsteroid.GetComponent<RotateAroundParent>().Speed = Random.Range(5, 9);

		return goAsteroid;
	}

	private GalaxyData GenerateGalaxyFromData(GalaxyData galaxyData)
	{
		foreach (StarSystemData starSystemData in galaxyData.StarSystems)
		{
			GameObject goStarSystem = GenerateStarSystemFromData(_space.transform, starSystemData);

			foreach (Discoverable child in goStarSystem.GetComponentsInChildren<Discoverable>())
				child.ToggleVisibility(starSystemData.Explored, starSystemData.Visible);

			GalaxyManager.Instance.AllStarSystems.Add(goStarSystem.GetComponent<StarSystem>());
		}

		return galaxyData;
	}

	private GameObject GenerateStarSystemFromData(Transform galaxy, StarSystemData starSystemData)
	{
		int asteroidCount = Random.Range(5, 10);

		// Generate new star system
		GameObject goStarSystem = Instantiate(StarSystemPrefab);
		var starSystem = goStarSystem.GetComponent<StarSystem>();

		// Attach data
		starSystem.Data = starSystemData;

		// Parent the star system to the galaxy
		goStarSystem.transform.SetParent(galaxy);

		// Apply origin point
		goStarSystem.transform.position = starSystemData.OriginPoint;

		// Generate name for new star system
		goStarSystem.name = starSystemData.Name;

		// Find star system's star
		GameObject goStar = goStarSystem.transform.Find("Star").gameObject;

		// Apply star size
		goStar.transform.localScale = Vector2.one * starSystemData.StarSize;

		// Assign type of star
		var starSpriteRenderer = goStar.GetComponent<SpriteRenderer>();
		var spriteName = starSystemData.Type.ToString();
		starSpriteRenderer.sprite = _starSprites.First(sp => sp.name == spriteName);

		foreach (PlanetData planetData in starSystemData.Planets)
		{
			GeneratePlanetFromData(planetData, goStarSystem);
		}

		for (int i = 0; i <= asteroidCount; i++)
		{
			//GameObject goAsteroid = GenerateNewAsteroid(goStarSystem);
		}

		return goStarSystem;
	}

	private GameObject GeneratePlanetFromData(PlanetData planetData, GameObject goStarSystem)
	{
		GameObject goPlanet = Instantiate(PlanetPrefab, goStarSystem.transform);

		var planetSpriteRenderer = goPlanet.GetComponent<SpriteRenderer>();
		var planet = goPlanet.GetComponent<Planet>();

		// Attach planet data
		planet.Data = planetData;

		// Assign name to gameobject from data
		goPlanet.name = planetData.Name;

		// Random planet size
		goPlanet.transform.localScale = Vector2.one * planetData.Size;

		// Assign sprite for planet
		var spriteName = planetData.Type.ToString();
		planetSpriteRenderer.sprite = _planetSprites.First(sp => sp.name == spriteName);

		// Generate random start position (random point around circumference of circle)
		ApplyStartPointInOrbit(goPlanet, planetData.OrbitRadius);

		// Set random orbit speed
		goPlanet.GetComponent<RotateAroundParent>().Speed = planetData.OrbitSpeed;

		// Activate the planet if the system is explored by the player
		if (!planetData.StarSystem.Explored)
		{
			goPlanet.GetComponent<SpriteRenderer>().enabled = false;
		}

		return goPlanet;
	}

	private void GenerateStarSystemConnections(IEnumerable<StarSystem> starSystems)
	{
		float connectWithin = 50;
		float connectWithinSqr = connectWithin * connectWithin;

		// TODO: Star system connection performance?
		foreach (StarSystem starSystem in starSystems)
		{
			Vector3 position = starSystem.transform.position;

			foreach (StarSystem neighbour in starSystems)
			{
				Vector3 neighbourPosition = neighbour.transform.position;
				float sqrDistance = (position - neighbourPosition).sqrMagnitude;
				if (!(sqrDistance < connectWithinSqr))
					continue;

				starSystem.Connections.Add(neighbour);

				// TODO: Refactor this to prefab
				GameObject goHyperlane = Instantiate(HyperlanePrefab, starSystem.transform);
				goHyperlane
					.GetComponent<LineRenderer>()
					.SetPositions(new[]
					{
						position - (position - neighbourPosition).normalized * 1.5f,
						neighbourPosition - (neighbourPosition - position).normalized * 1.5f
					});

				goHyperlane.GetComponent<Discoverable>().ToggleVisibility(starSystem);
			}
		}
	}

	private void ApplyStartPointInOrbit(GameObject target, float radius)
	{
		float angle = Random.value * Mathf.PI * 2.0f;
		target.transform.localPosition = new Vector2(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius);
	}
}
