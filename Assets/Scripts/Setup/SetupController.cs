﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SetupController : MonoBehaviour
{
	[SerializeField]
	private Transform _options;

	public GameObject SliderSettingPrefab;

	public Dictionary<string, FloatSetting> IntSettings = new Dictionary<string, FloatSetting>
	{
		//{ "Empires", new FloatSetting(2.0f) { MinValue = 1, MaxValue = 3 } },
		{ "Galaxies", new FloatSetting(15.0f) { MinValue = 5, MaxValue = 100, SliderMultiplier = 5.0f } }
	};

	private void Start()
	{
		foreach (string key in IntSettings.Keys)
		{
			FloatSetting setting = IntSettings[key];

			// Create new slider control
			GameObject sliderGameObject = Instantiate(SliderSettingPrefab, _options);

			// Name the control
			sliderGameObject.name = key;

			// Set the label name/description
			sliderGameObject.transform
			 .Find("Label")
			 .Find("Name")
			 .GetComponent<TextMeshProUGUI>().text = key;

			// Find the slider control for this int setting
			var slider = sliderGameObject
				.GetComponentInChildren<Slider>();

			// Attach on change event to slider
			slider.onValueChanged.AddListener(val => OnIntSettingChanged(key, val));

			// Set value of the slider to default value
			slider.value = setting.Value / setting.SliderMultiplier;

			// Set slider bounds
			slider.minValue = setting.MinValue / setting.SliderMultiplier;
			slider.maxValue = setting.MaxValue / setting.SliderMultiplier;
		}
	}

	private void OnIntSettingChanged(string settingName, float value)
	{
		FloatSetting setting = IntSettings[settingName];

		setting.Value = value * setting.SliderMultiplier;

		UpdateLabelValue(settingName, setting.GetDisplayValue());
	}

	private void UpdateLabelValue(string setting, string value)
	{
		_options
			.Find(setting)
			.Find("Label")
			.Find("Value")
			.GetComponent<TextMeshProUGUI>().text = value;
	}

	public void StartGame()
	{
		SceneManager.LoadScene(2);
	}

	public void CancelSetup()
	{
		SceneManager.LoadScene(0);
	}
}

public abstract class Setting
{
	public abstract string GetDisplayValue();
}


public class FloatSetting : Setting
{
	public FloatSetting() { }
	public FloatSetting(float value) { Value = value; }

	public float Value { get; set; }
	public float MinValue { get; set; }
	public float MaxValue { get; set; }

	public float SliderMultiplier { get; set; } = 1.0f;

	public override string GetDisplayValue()
	{
		return Value.ToString();
	}
}

