﻿using System;
using UnityEngine;

public class SpaceStationGenerator : MonoBehaviour
{
	public static SpaceStationGenerator Instance { get; set; }

	public static Action<GameObject> OnSpaceStationCreated;

	public GameObject SpaceStationPrefab;

	void Awake()
	{
		Instance = this;
	}

	public SpaceStation CreateSpaceStation(StarSystem starSystem, EmpireData owner)
	{
		GameObject goSpaceStation = Instantiate(SpaceStationPrefab, starSystem.transform);
		SpaceStation spaceStation = goSpaceStation.GetComponent<SpaceStation>();

		spaceStation.name = starSystem.Data.Name + " Station";
		spaceStation.Owner = owner;

		OnSpaceStationCreated?.Invoke(goSpaceStation);

		return spaceStation;
	}
}
