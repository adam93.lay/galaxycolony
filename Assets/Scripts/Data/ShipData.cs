﻿using Newtonsoft.Json;

public class ShipData
{
	public int Id { get; set; }
	public string Name { get; set; }
	public ShipTypes Type { get; set; }
	public float Speed { get; set; }

	public int OwnerEmpireId { get; set; }
	[JsonIgnore]
	public EmpireData Owner { get; set; }
}

public enum ShipTypes
{
	Standard
}
