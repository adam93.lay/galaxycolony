﻿using Newtonsoft.Json;

public class PlanetData
{
	public int Id { get; set; }
	public string Name { get; set; }
	public float Size { get; set; }
	public PlanetTypes Type { get; set; }
	public float OrbitRadius { get; set; }
	public float OrbitSpeed { get; set; }

	public int StarSystemId { get; set; }
	[JsonIgnore]
	public StarSystemData StarSystem { get; set; }
}

public enum PlanetTypes
{
	Blue,
	Cyan,
	Dark,
	EarthLike,
	GasBlue,
	GasRed,
	GasViolet,
	Ice,
	Lava,
	Orange,
	Purple,
	Red,
	Sand,
	Water
}