﻿public class StarSystemData
{
	public int Id { get; set; }
	public string Name { get; set; }
	public StarSystemTypes Type { get; set; }
	public SerializableVector3 OriginPoint { get; set; }
	public float StarSize { get; set; }
	public bool Explored { get; set; }
	public bool Visible { get; set; }

	public PlanetData[] Planets { get; set; }
}

public enum StarSystemTypes
{
	Yellow,
	White
}
