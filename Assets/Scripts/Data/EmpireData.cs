﻿using Newtonsoft.Json;

public class EmpireData
{
	public int Id { get; set; }
	public string Name { get; set; }
	public PlayerTypes PlayerType { get; set; }

	public ShipData[] Ships { get; set; }

	public int HomeSystemId { get; set; }
	[JsonIgnore]
	public StarSystemData HomeSystem { get; set; }
}

public enum PlayerTypes
{
	Player,
	AI,
	Online // lol
}
