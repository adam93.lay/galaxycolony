﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

public static class SaveManager
{
	private const string PATH = @"C:\temp\galaxy_save_01.json";
	public static List<ISaveState> ToSave = new List<ISaveState>();

	public static GlobalState LoadState;

	public static void Register(ISaveState obj)
	{
		if (!ToSave.Contains(obj))
			ToSave.Add(obj);
	}

	private static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
	{
		TypeNameHandling = TypeNameHandling.All,
		Formatting = Formatting.Indented
	};

	public static void SaveGame()
	{
		var globalState = new GlobalState();

		foreach (ISaveState toSave in ToSave)
		{
			toSave.SaveState(globalState);
		}

		File.WriteAllText(PATH, JsonConvert.SerializeObject(globalState, _jsonSerializerSettings));
	}

	public static void LoadGame()
	{
		string text = File.ReadAllText(PATH);

		LoadState = JsonConvert.DeserializeObject<GlobalState>(text, _jsonSerializerSettings);
	}
}
