﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Follow : MonoBehaviour
{
	public Transform Target;
	public Vector3 Offset;

	void Update()
	{
		transform.position = Target.position + Offset;
	}
}
