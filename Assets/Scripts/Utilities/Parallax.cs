﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
	public Camera Camera;
	public float ParallaxAmount;

	private Vector2 _startPos;
	private Vector2 _size;

	private void Start()
	{
		_startPos = transform.position;
		_size = GetComponent<SpriteRenderer>().bounds.size / 3;
	}

	private void FixedUpdate()
	{
		Vector2 a = Camera.transform.position * (1 - ParallaxAmount);
		Vector2 distance = Camera.transform.position * ParallaxAmount;

		transform.position = _startPos + distance;

		// Scroll x
		if (a.x > _startPos.x + _size.x)
		{
			_startPos = new Vector2(_startPos.x + _size.x, _startPos.y);
		}
		else if (a.x < _startPos.x - _size.x)
		{
			_startPos = new Vector2(_startPos.x - _size.x, _startPos.y);
		}
		// Scroll y
		if (a.y > _startPos.y + _size.y)
		{
			_startPos = new Vector2(_startPos.x, _startPos.y + _size.y);
		}
		else if (a.y < _startPos.y - _size.y)
		{
			_startPos = new Vector2(_startPos.x, _startPos.y - _size.y);
		}
	}
}
