﻿using System;
using UnityEngine;

public class RotateAroundTarget : MonoBehaviour
{
  public Transform Target;
  public float Speed;
  public float Radius;

  private const float PI2 = Mathf.PI * 2;

  private float _t;

  private void LateUpdate()
  {
    _t += Time.deltaTime * Speed * (float)Math.PI;
    if (_t > PI2)
      _t = 0;

    float x = Radius * Mathf.Cos(_t);
    float y = Radius * Mathf.Sin(_t);

    var pos = new Vector3(x, y, 0);

    transform.position = Target.position + pos;
    transform.Rotate(Vector3.forward, Time.deltaTime * 180 * Speed);
  }

  public void SetTimeFromSignedAngle(float degrees)
  {
    float offset = 180f;
    degrees = (degrees + 360f + offset) % 360f;
    float prc = degrees / 360f;
    _t = PI2 * prc;
  }
}
