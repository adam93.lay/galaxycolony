﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundParent : MonoBehaviour
{
  public float Speed;

  private void Update()
  {
    transform.RotateAround(transform.parent.position, Vector3.forward, Time.deltaTime * Speed);
  }
}
