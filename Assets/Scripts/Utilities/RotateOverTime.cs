﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

public class RotateOverTime : MonoBehaviour
{
	public float SpeedZ;

	void Update()
	{
		transform.Rotate(Vector3.forward, SpeedZ * Time.deltaTime);
	}
}
