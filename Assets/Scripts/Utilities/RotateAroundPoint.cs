﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundPoint : MonoBehaviour
{
  public Vector2 Point;
  public float Speed;
  public bool MaintainOrientation;

  private void Update()
  {
    transform.RotateAround(Point, Vector3.forward, Time.deltaTime * Speed);

    if (MaintainOrientation)
      transform.rotation = Quaternion.identity;
  }
}
