﻿using System.Collections.Generic;
using UnityEngine;

public static class ResourceExtensions
{
  public static Dictionary<ResourceTypes, string> ResourceIcons = new Dictionary<ResourceTypes, string>
  {
    { ResourceTypes.Food, "Images/UI/Icons/Resources/food" },
    { ResourceTypes.Steel, "Images/UI/Icons/Resources/steel" },
    { ResourceTypes.Titanium, "Images/UI/Icons/Resources/titanium" },
    { ResourceTypes.Uranium, "Images/UI/Icons/Resources/uranium" },
    { ResourceTypes.Population, "Images/UI/Icons/Resources/population" },
    { ResourceTypes.Credits, "Images/UI/Icons/Resources/steel" }
  };

  public static string GetImagePath(this ResourceTypes type) => ResourceIcons[type];
  public static Sprite GetSprite(this ResourceTypes type) => Resources.Load<Sprite>(type.GetImagePath());

  public static Dictionary<ResourceTypes, Color32> ResourceColours = new Dictionary<ResourceTypes, Color32>
  {
    { ResourceTypes.Food, new Color32(192, 255, 127, 255) },
    { ResourceTypes.Steel, new Color32(220, 220, 255, 255) },
    { ResourceTypes.Titanium, new Color32(220, 255, 220, 255) },
    { ResourceTypes.Uranium, new Color32(100, 220, 100, 255) },
    { ResourceTypes.Population, Color.white },
    { ResourceTypes.Credits, Color.white }
  };

  public static Color GetColor(this ResourceTypes type) => ResourceColours[type];
}
