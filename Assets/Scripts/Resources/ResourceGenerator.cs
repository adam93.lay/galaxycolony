﻿using UnityEngine;

public class ResourceGenerator : MonoBehaviour
{
  [SerializeField]
  private ResourceStore _output;
  public ResourceStore Output => _output;

  [SerializeField]
  private ResourceTypes _generateType;
  public ResourceTypes GenerateType => _generateType;

  private void Start()
  {
    TimeManager.OnCycleChanged += HandleCycleChanged;
  }

  private void HandleCycleChanged(int _)
  {
    Resource res = Output?.Get(GenerateType);

    if (res == null)
    {
      print("Store does not have resource");
      return;
    }

    res.TryAddValue(1, out _);
  }

  public void Init(ResourceStore output, ResourceTypes type)
  {
    _output = output;
    _generateType = type;
  }
}
