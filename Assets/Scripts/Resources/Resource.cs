﻿using System;

[Serializable]
public class Resource
{
	public ResourceTypes Type;
	public int? MaxValue = 25;
	public int Value { get; private set; }

	public Action<Resource> OnChanged;

	public bool TryAddValue(int amount) => TryAddValue(amount, out _);
	public bool TryAddValue(int amount, out int value)
	{
		if (MaxValue.HasValue == false || Value + amount <= MaxValue)
		{
			value = (Value += amount);
			OnChanged?.Invoke(this);
			return true;
		}

		value = Value;
		return false;
	}

	public bool TryTakeValue(int amount) => TryTakeValue(amount, out _);
	public bool TryTakeValue(int amount, out int value)
	{
		if (Value >= amount)
		{
			value = (Value -= amount);
			OnChanged?.Invoke(this);
			return true;
		}

		value = Value;
		return false;
	}
}

public enum ResourceTypes
{
	Credits,
	Food,
	Steel,
	Titanium,
	Uranium,
	Population
}