﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResourceStore : MonoBehaviour
{
  public Resource[] Resources;

  public Resource Get(ResourceTypes type)
  {
    return Resources.FirstOrDefault(r => r.Type == type);
  }

  public bool Transfer(ResourceStore to, ResourceTypes type, int amount)
  {
	  Resource fromResource = Get(type);
	  Resource toResource = to.Get(type);

    // if we don't have this resource, or nowhere to send it...
	  if (fromResource == null || toResource == null)
		  return false;

    // If there's not enough / we fail to take the resource
    if (!fromResource.TryTakeValue(amount))
	    return false;

    // if we fail to add the resource...
    if (!toResource.TryAddValue(amount))
    {
      // ...then put it back in the original
      fromResource.TryAddValue(amount);
      return false;
    }

    return true;
  }
}
