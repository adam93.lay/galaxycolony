﻿using UnityEngine;

public class GameMenuManager : MonoBehaviour
{
	public GameObject AssignmentsWindow;

	public void ToggleAssignmentsWindow()
	{
		AssignmentsWindow.SetActive(!AssignmentsWindow.activeSelf);
	}
}
