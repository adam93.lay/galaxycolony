﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class AlertManager : MonoBehaviour
{
	public GameObject AlertList;
	public GameObject AlertItemPrefab;
	public int MaxAlerts = 5;
	public List<AlertItem> CurrentAlerts = new List<AlertItem>();

	private void OnStarSystemExplored(StarSystem starSystem)
	{
		CreateAlert(starSystem.Data.Name + "Explored!", "You've explored a new system!");
	}

	private void Start()
	{
		AlertItem.OnDismissed += OnDismissed;

		StarSystem.OnStarSystemExplored += OnStarSystemExplored;

		gameObject.SetActive(false);
	}

	private void OnDismissed(AlertItem alertItem)
	{
		CurrentAlerts.Remove(alertItem);

		// Hide panel when no notifications left
		if (!CurrentAlerts.Any())
			gameObject.SetActive(false);
	}

	public void CreateAlert(string title, string body)
	{
		// If there aren't any alerts then panel will be disabled
		if (!CurrentAlerts.Any())
			gameObject.SetActive(true);

		GameObject goAlertItem = Instantiate(AlertItemPrefab, AlertList.transform);

		goAlertItem.transform.Find("Title").GetComponent<TextMeshProUGUI>().text = title;
		goAlertItem.transform.Find("Body").GetComponent<TextMeshProUGUI>().text = body;

		CurrentAlerts.Add(goAlertItem.GetComponent<AlertItem>());

		if (CurrentAlerts.Count > MaxAlerts)
		{
			CurrentAlerts.First().Remove();
		}
	}
}
