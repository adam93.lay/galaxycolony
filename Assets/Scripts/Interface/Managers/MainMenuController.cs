﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
  public void NewGame()
  {
    SceneManager.LoadScene(1);
  }
}
