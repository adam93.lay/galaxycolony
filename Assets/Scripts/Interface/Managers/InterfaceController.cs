﻿using System.Linq;
using TMPro;
using UnityEngine;

public class InterfaceController : MonoBehaviour
{
	public static InterfaceController Instance { get; private set; }

	public GameObject OutlineWindow;
  public GameObject PlanetHoverUIPrefab;

  public TextMeshProUGUI CycleText;
  public TextMeshProUGUI GalaxyText;
  public Tooltip Tooltip;

  public bool ZoomedOut = false;

  private void Awake()
  {
	  Instance = this;

	  TimeManager.OnCycleChanged += HandleCycleChanged;
	  GalaxyManager.OnStarSystemChanged += HandleGalaxyChanged;
  }

  private void Start()
  {
    Init();
  }

  private void Init()
  {
    HandleCycleChanged(TimeManager.Instance.CurrentCycle);

    //foreach (StarSystem starSystem in GalaxyManager.Instance.AllStarSystems)
    //{
	   // Planet[] planets = starSystem.GetComponentsInChildren<Planet>();

	   // foreach (Planet planet in planets)
	   // {
		  //  GameObject ui = Instantiate(PlanetHoverUIPrefab, transform.Find("PlanetUI"));
		  //  ui.GetComponent<PlanetHoverUI>().Planet = planet;
    //  }
    //}
  }

  private void HandleCycleChanged(int currentCycle)
  {
    string suffix = "th";
    switch (currentCycle.ToString().Last())
    {
      case '1': suffix = "st";break;
      case '2': suffix = "nd"; break;
    }

    // Update cycle text
    CycleText.text = $"{currentCycle}{suffix} Cycle";
  }

  private void HandleGalaxyChanged(StarSystem starSystem)
  {
	  GalaxyText.text = starSystem?.Data.Name;
  }
}
