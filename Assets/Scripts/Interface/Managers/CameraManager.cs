﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraManager : MonoBehaviour, ISaveState
{
	public static CameraManager Instance { get; private set; }

	public Camera _camera;
	public float MoveSpeed;
	public float ZoomSpeed;

	public class CameraManagerState
	{
		public SerializableVector3 LastPosition;
		public float LastZoom { get; set; }
	}

	void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		_camera = GetComponent<Camera>();

		SaveManager.Register(this);

		if (SaveManager.LoadState != null)
		{
			CameraManagerState loadState = SaveManager.LoadState.CameraManager;
			transform.position = loadState.LastPosition;
			_camera.orthographicSize = loadState.LastZoom;
		}
	}

	private Vector2? _lastMousePos;
	private StarSystem _lastStarSystem;

	private Coroutine _zoomCoroutine;
	private IEnumerator Zoom(float scrollIncrement)
	{
		float zoomStart = _camera.orthographicSize;
		float zoomEnd = Mathf.Clamp(zoomStart + scrollIncrement, 5f, 50f);
		float time = 0;
		float totalTime = 0.25f;

		while (time < totalTime)
		{
			time += Time.deltaTime;
			_camera.orthographicSize = Mathf.Lerp(zoomStart, zoomEnd, time / totalTime);
			yield return null;
		}

		_zoomCoroutine = null;
	}

	private Coroutine _moveToCoroutine;
	private IEnumerator MoveTo(Vector3 location)
	{
		Vector3 moveStart = _camera.transform.position;
		Vector3 moveEnd = location;
		float time = 0;
		float totalTime = 0.25f;

		while (time < totalTime)
		{
			time += Time.deltaTime;
			_camera.transform.position = Vector3.Lerp(moveStart, moveEnd, time / totalTime);
			yield return null;
		}

		_moveToCoroutine = null;
	}

	private void Update()
	{
		#region Zooming

		// TODO: Refactor into coroutine?

		float scroll = Input.GetAxis("Mouse ScrollWheel");
		float scrollIncrement = -scroll * ZoomSpeed * (Input.GetKey(KeyCode.LeftControl) ? 10f : 1f);

		if (scrollIncrement != 0)
		{
			if (_zoomCoroutine == null)
			{
				_zoomCoroutine = StartCoroutine(Zoom(scrollIncrement));
			}
		}

		#endregion

		#region Pan camera

		var mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

		if (Input.GetMouseButtonDown((int)MouseButton.MiddleMouse))
		{
			_lastMousePos = mousePos;
		}
		else if (Input.GetMouseButton((int)MouseButton.MiddleMouse))
		{
			Vector3 diff = _lastMousePos.Value - mousePos;

			_camera.transform.position += diff * MoveSpeed * (_camera.orthographicSize * 0.25f);

			_lastMousePos = mousePos;
		}

		#endregion

		#region Adjust UI on zoom level

		// Zoomed in
		if (_camera.orthographicSize < 25)
		{
			InterfaceController.Instance.ZoomedOut = false;

			// See if we have panned over into a new system
			StarSystem starSystem = GetClosestStarSystem(GalaxyManager.Instance.AllStarSystems);
			if (starSystem != _lastStarSystem)
			{
				_lastStarSystem = starSystem;
				GalaxyManager.OnStarSystemChanged?.Invoke(_lastStarSystem);
			}
		}
		// Zoomed out
		else
		{
			InterfaceController.Instance.ZoomedOut = true;

			if (_lastStarSystem != null)
			{
				_lastStarSystem = null;
				GalaxyManager.OnStarSystemChanged?.Invoke(_lastStarSystem);
			}
		}

		#endregion
	}

	public void FocusOn(GameObject target)
	{
		// Zoom in on target
		_camera.orthographicSize = 10.0f;

		// Destination is the target's location
		var destination = new Vector3(target.transform.position.x, target.transform.position.y, -10f);

		// If already moving, then stop...
		if (_moveToCoroutine != null)
			StopCoroutine(_moveToCoroutine);

		// ...and move to new location
		_moveToCoroutine = StartCoroutine(MoveTo(destination));
	}

	private StarSystem GetClosestStarSystem(IEnumerable<StarSystem> starSystems)
	{
		StarSystem tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (StarSystem system in starSystems)
		{
			float dist = Vector3.Distance(system.transform.position, currentPos);
			if (dist < minDist)
			{
				tMin = system;
				minDist = dist;
			}
		}
		return tMin;
	}

	public void SaveState(GlobalState state)
	{
		state.CameraManager = new CameraManagerState
		{
			LastPosition = transform.position,
			LastZoom = _camera.orthographicSize
		};
	}
}
