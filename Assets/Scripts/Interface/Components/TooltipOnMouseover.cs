﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipOnMouseover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public string Text;

	public void OnPointerEnter(PointerEventData eventData) => OnMouseEnter();
	void OnMouseEnter()
	{
		InterfaceController.Instance.Tooltip.SetText(String.IsNullOrEmpty(Text) ? name : Text);
		InterfaceController.Instance.Tooltip.gameObject.SetActive(true);
	}

	public void OnPointerExit(PointerEventData eventData) => OnMouseExit();
	void OnMouseExit()
	{
		InterfaceController.Instance.Tooltip.gameObject.SetActive(false);
	}
}
