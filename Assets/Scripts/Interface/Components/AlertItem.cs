﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class AlertItem : MonoBehaviour, IPointerClickHandler
{
	public static Action<AlertItem> OnDismissed;

	public void OnPointerClick(PointerEventData eventData)
	{
		Remove();
	}

	public void Remove()
	{
		OnDismissed?.Invoke(this);

		Destroy(gameObject);
	}
}
