﻿using TMPro;
using UnityEngine;

public class PlanetPanelInterface : MonoBehaviour
{
  public TextMeshProUGUI SelectedPlanetText;

  private void Start()
  {
    PlanetManager.OnPlanetSelectionChanged += HandlePlanetSelectionChanged;

    Init();
  }

  private void Init()
  {
    gameObject.SetActive(false);
    SelectedPlanetText.text = string.Empty;
  }

  private void HandlePlanetSelectionChanged(Planet planet)
  {
    SelectedPlanetText.text = planet?.Data.Name ?? string.Empty;

    if (planet == null)
    {
      gameObject.SetActive(false);
      return;
    }

    gameObject.SetActive(true);
  }
}
