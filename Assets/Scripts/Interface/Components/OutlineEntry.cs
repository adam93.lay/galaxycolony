﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

public class OutlineEntry : MonoBehaviour, IPointerClickHandler
{
	public GameObject Target;

	
	public float? _lastClickTime = null;

	public void OnPointerClick(PointerEventData eventData)
	{
		//float timeSinceLastClick = Time.time - (_lastClickTime ?? 0);

		if (_lastClickTime.HasValue)
		{
			float timeSinceLastClick = Time.time - _lastClickTime.Value;

			// First click
			if (timeSinceLastClick > 1.0f)
			{
				SelectTarget();
			}
			// Second click
			else
			{
				FocusOnTarget();
			}
		}
		// First click
		else
		{
			SelectTarget();
		}

		_lastClickTime = Time.time;
	}

	private void SelectTarget()
	{
		if (Target.GetComponent<Selectable>() is Selectable s)
		{
			s.Select();
		}
	}

	private void FocusOnTarget()
	{
		CameraManager.Instance.FocusOn(Target);
	}
}
