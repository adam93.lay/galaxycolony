﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[Obsolete]
public class ColonyPanelInterface : MonoBehaviour
{
  private Button _spawnPopButton;

  private void Start()
  {
    _spawnPopButton = transform.Find("SpawnPopButton").GetComponent<Button>();

    PlanetManager.OnPlanetSelectionChanged += HandlePlanetSelectionChanged;

    Init();
  }

  private void Init()
  {
    gameObject.SetActive(false);
  }

  private void HandlePlanetSelectionChanged(Planet planet)
  {
    var colony = planet?.transform.GetComponentInChildren<Colony>();

    if (planet == null || colony == null)
    {
      gameObject.SetActive(false);
      return;
    }

    gameObject.SetActive(true);

    var colonyResources = colony.GetComponent<ResourceStore>();

    if (colonyResources == null)
      return;

    if (planet.AvailableResourceTypes.Contains(ResourceTypes.Food))
    {
      _spawnPopButton.gameObject.SetActive(true);
      _spawnPopButton.onClick = new Button.ButtonClickedEvent();
      _spawnPopButton.onClick.AddListener(() =>
      {
        // TODO: Move this logic
        Resource colonyFoodRes = colonyResources.Get(ResourceTypes.Food);

        if (colonyFoodRes.TryTakeValue(5, out _))
        {
          colonyResources.Get(ResourceTypes.Population).TryAddValue(1, out _);
        }
      });
    }
    else
    {
      _spawnPopButton.gameObject.SetActive(false);
    }
  }
}
