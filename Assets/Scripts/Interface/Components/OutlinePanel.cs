﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OutlinePanel : MonoBehaviour
{
	public GameObject OutlineEntryPrefab;
	public GameObject SpaceStationsList;
	public GameObject ShipsList;

	void Awake()
	{
		ShipGenerator.OnShipCreated += HandleShipCreated;
		SpaceStationGenerator.OnSpaceStationCreated += HandleSpaceStationCreated;
	}

	private void HandleSpaceStationCreated(GameObject spaceStation)
	{
		Sprite icon = spaceStation.GetComponent<SpriteRenderer>().sprite;
		CreateEntry(icon, spaceStation.name, SpaceStationsList, spaceStation);
	}

	private void HandleShipCreated(Ship ship)
	{
		Sprite icon = ship.GetComponent<SpriteRenderer>().sprite;
		CreateEntry(icon, ship.Data.Name, ShipsList, ship.gameObject);
	}

	private GameObject CreateEntry(Sprite iconSprite, string name, GameObject parent, GameObject target)
	{
		GameObject goEntry = Instantiate(OutlineEntryPrefab, parent.transform);
		goEntry.GetComponent<OutlineEntry>().Target = target;

		var icon = goEntry.transform.Find("Icon").GetComponent<Image>();
		var text = goEntry.transform.Find("Name").GetComponent<TextMeshProUGUI>();

		icon.sprite = iconSprite;
		text.text = name;

		return goEntry;
	}
}
