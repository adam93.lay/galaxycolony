﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : EventTrigger
{
	public Transform Target;

	private bool _dragging;
	private Vector2 _offset;

	public void Update()
	{
		if (_dragging)
		{
			Target.transform.position = _offset + new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		}
	}

	public override void OnPointerDown(PointerEventData eventData)
	{
		_offset = (Vector2)Target.transform.position - new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		_dragging = true;
	}

	public override void OnPointerUp(PointerEventData eventData)
	{
		_dragging = false;
	}
}
