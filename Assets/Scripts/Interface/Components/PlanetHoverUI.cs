﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlanetHoverUI : MonoBehaviour
{
  public float Offset = 0.25f;
  public TextMeshProUGUI Name;
  public Planet Planet;
  public bool Visible = false;

  private CanvasGroup _canvasGroup;

  private void Start()
  {
	  _canvasGroup = GetComponent<CanvasGroup>();

	  Planet = transform.parent.parent.GetComponent<Planet>();

    Name.text = Planet.Data.Name;

    Transform resourceIcons = transform.Find("ResourceIcons");

    foreach (ResourceTypes type in Planet.AvailableResourceTypes)
    {
      GameObject icon = CreateResourceIcon(type, resourceIcons);
    }
  }

  private void LateUpdate()
  {
    //transform.position = Camera.main.WorldToScreenPoint(Planet.transform.position + (Vector3)Vector2.one * (Planet.Data.Size));

    // Offset parent rotation to remain square to camera
    transform.parent.localRotation = Quaternion.Inverse(transform
	    .parent // Canvas
	    .parent // Planet
	    .rotation);

	  _canvasGroup.alpha = !Visible || InterfaceController.Instance.ZoomedOut ? 0f : 1f;
  }

  private GameObject CreateResourceIcon(ResourceTypes type, Transform parent)
  {
    var icon = new GameObject(type.ToString());
    
    var image = icon.AddComponent<Image>();
    image.sprite = type.GetSprite();
    image.color = type.GetColor();

    icon.transform.SetParent(parent);
    icon.GetComponent<RectTransform>().sizeDelta = new Vector2(20f, 20f);

    return icon;
  }
}
