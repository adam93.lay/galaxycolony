﻿using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
	public Vector3 Offset;

	private TextMeshProUGUI _text;
	private TextMeshProUGUI Text => _text ?? (_text = transform.GetComponentInChildren<TextMeshProUGUI>());

	public void SetText(string text)
	{
		Text.text = text;
	}

	void Update()
	{
		if (!gameObject.activeSelf)
			return;

		// Follow the mouse
		transform.position = Input.mousePosition + Offset;
	}
}