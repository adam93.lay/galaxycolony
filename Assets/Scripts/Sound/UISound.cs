﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UISound : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
{
	public AudioClip SoundOnClick;
	public AudioClip SoundOnMouseOver;

	public void OnPointerClick(PointerEventData eventData)
	{
		if (SoundOnClick != null)
			AudioSource.PlayClipAtPoint(SoundOnClick, transform.position);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (SoundOnMouseOver != null)
			AudioSource.PlayClipAtPoint(SoundOnMouseOver, Vector3.zero, 1);
	}
}
